import { writable } from "svelte/store";


export enum Page {
    login = "login",
    main = "main",
    error = "error",
    timer = "timer"
}
export const page = writable(Page.login)